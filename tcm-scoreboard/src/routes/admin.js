var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {     
  res.render('index', { });
});

var person_controller = require('../controllers/personController');

router.get('/people', person_controller.person_list);
router.get('/people/new', person_controller.person_create_get);
router.post('/people/new', person_controller.person_create_post);
router.get('/people/:id/delete', person_controller.person_delete_get);

var goal_controller = require('../controllers/goalController');

router.get('/goals', goal_controller.goal_list);
router.get('/goals/new', goal_controller.goal_create_get);
router.post('/goals/new', goal_controller.goal_create_post);
router.get('/goals/:id/delete', goal_controller.goal_delete_get);

var lag_measure_controller = require('../controllers/lagMeasureController');

router.get('/lag_measures', lag_measure_controller.lag_measure_list);
router.get('/lag_measures/new', lag_measure_controller.lag_measure_create_get);
router.post('/lag_measures/new', lag_measure_controller.lag_measure_create_post);
router.get('/lag_measures/:id/delete', lag_measure_controller.lag_measure_delete_get);

var lead_measure_controller = require('../controllers/leadMeasureController');

router.get('/lead_measures', lead_measure_controller.lead_measure_list);
router.get('/lead_measures/new', lead_measure_controller.lead_measure_create_get);
router.post('/lead_measures/new', lead_measure_controller.lead_measure_create_post);
router.get('/lead_measures/:id/delete', lead_measure_controller.lead_measure_delete_get);

var commitment_controller = require('../controllers/commitmentController');

router.get('/commitments', commitment_controller.commitment_list);
router.get('/commitments/:id/delete', commitment_controller.commitment_delete_get);

module.exports = router;