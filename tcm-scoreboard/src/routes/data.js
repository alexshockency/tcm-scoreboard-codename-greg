var express = require('express');
var router = express.Router();

var person_controller = require('../controllers/personController');
var goal_controller = require('../controllers/goalController');
var lag_measure_controller = require('../controllers/lagMeasureController');
var lead_measure_controller = require('../controllers/leadMeasureController');
var commitment_controller = require('../controllers/commitmentController');

router.get('/people', person_controller.load_people);
router.get('/goals', goal_controller.load_goals);
router.get('/lag_measures', lag_measure_controller.load_lag_measures);
router.post('/lag_measures/update', lag_measure_controller.update_lag_measure);
router.get('/lead_measures', lead_measure_controller.load_lead_measures);
router.get('/commitments', commitment_controller.load_commitments);
router.post('/commitments/create', commitment_controller.create_commitment);
router.post('/commitments/update', commitment_controller.update_commitment);

module.exports = router;