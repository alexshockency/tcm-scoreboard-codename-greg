// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const apiServerUrl = window.location.origin.substring(0,+ window.location.origin.lastIndexOf(":"))+':4201'

export const environment = {
  production: false,
  api: {
    people: apiServerUrl + '/data/people',
    leadMeasures: apiServerUrl + '/data/lead_measures',
    lagMeasures: apiServerUrl + '/data/lag_measures',
    commitments: apiServerUrl + '/data/commitments'
  }
}

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
