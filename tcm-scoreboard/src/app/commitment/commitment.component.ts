import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Commitment } from '../commitment';

@Component({
  selector: 'app-commitment',
  templateUrl: './commitment.component.html',
  styleUrls: ['./commitment.component.css']
})
export class CommitmentComponent implements OnChanges {
  @Output() statusChanged = new EventEmitter();
  @Input() commitment: Commitment;
  @Input() i: number;
  calculatedOpacity: number;

  constructor() { }

  ngOnChanges() {
    if (this.commitment.done)
       this.calculatedOpacity = Math.max((9 - this.i) / 10, .3)
    else
      this.calculatedOpacity = 1
  }

  onChange() {
    this.statusChanged.emit(this.commitment);
  }

}
