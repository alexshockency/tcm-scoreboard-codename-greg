import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TriageScoreboardComponent } from './triage-scoreboard.component';

describe('TriageScoreboardComponent', () => {
  let component: TriageScoreboardComponent;
  let fixture: ComponentFixture<TriageScoreboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TriageScoreboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TriageScoreboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
