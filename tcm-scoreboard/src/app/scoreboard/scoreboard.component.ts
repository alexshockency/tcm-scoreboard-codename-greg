import { Component, OnChanges, Input } from '@angular/core';
import { MatExpansionModule } from '@angular/material';
import { DataService } from '../data.service';
import { Commitment } from '../commitment';
import { LeadMeasure } from '../leadMeasure';
import { LagMeasure } from '../lagMeasure';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnChanges {
  @Input() title: string
  @Input() lagMeasures: LagMeasure[]
  @Input() names: string[]
  @Input() leadMeasures: LeadMeasure[]
  @Input() commitments: Commitment[]
  data: Array<Array<any>>

  constructor() { }

  ngOnChanges() {
    this.data = []

    for (let lagMeasure of this.filteredLagMeasures()) {
      this.data.push([lagMeasure.name, Number.parseInt(lagMeasure.value.toString())])
    }
  }

  filteredLeadMeasures() {
    return this.leadMeasures.filter(leadMeasure => leadMeasure.goal.name === this.title)
  }

  filteredLagMeasures() : LagMeasure[] {
    return this.lagMeasures.filter(lagMeasure => lagMeasure.goal.name === this.title)
  }
  
  getLeadMeasureCount(leadMeasureName: string) {
    return this.commitments.filter(commitment => commitment.leadMeasure.name === leadMeasureName && commitment.done).length
  }
  
  filteredNames(leadMeasureName: string) {
    return new Set(this.commitments.filter(commitment => commitment.leadMeasure.name === leadMeasureName && commitment.done).map(commitment => commitment.person.name))
  }
  
  getNameCount(leadMeasureName: string, name: string) {
    return this.commitments.filter(commitment => commitment.leadMeasure.name === leadMeasureName && commitment.person.name === name && commitment.done).length
  }

  getValue(lagMeasureName: string) {
    return this.lagMeasures.find(lagMeasure => lagMeasure.name === lagMeasureName).value
  }

  filteredParticipants(leadMeasureName: string) {
    let filteredCommitments = this.commitments.filter(commitment => commitment.leadMeasure.name === leadMeasureName && commitment.done)
    let filteredNames = new Set(filteredCommitments.map(commitment => commitment.person.name))
    let participants: Participant[] = []

    filteredNames.forEach((filteredName) => {
     participants.push({
       name: filteredName,
       count: filteredCommitments.filter(commitment => commitment.person.name === filteredName).length
     })
    })
    
    participants.sort((a, b) => {
      return b.count - a.count
    })
   
    return participants
  }

}

export class Participant {
  name: string
  count: number
}
