import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Person } from '../person';
import { Commitment } from '../commitment';
import { LeadMeasure } from '../leadMeasure';

@Component({
  selector: 'app-commitment-log',
  templateUrl: './commitment-log.component.html',
  styleUrls: ['./commitment-log.component.css']
})
export class CommitmentLogComponent {
  @Output() commitmentChanged = new EventEmitter();
  @Output() commitmentCreated = new EventEmitter();
  @Input() people: Person[]
  @Input() leadMeasures: LeadMeasure[]
  @Input() commitments: Commitment[]
  newCommitment: Commitment = new Commitment()

  constructor() { }
  
  onSubmit() {
    this.newCommitment.done = false
    
    if (!this.newCommitment.person || !this.newCommitment.leadMeasure || !this.newCommitment.statement)
      return;

    this.commitmentCreated.emit(this.newCommitment)

    this.newCommitment = new Commitment()
  }
  
  getCommitmentsByStatus(done: boolean) {
    if (done)
      return this.commitments.filter(commitment => commitment.done == done).slice(0, 25)
    else
      return this.commitments.filter(commitment => commitment.done == done)
  }

  statusChanged(commitment: Commitment) {
    this.commitmentChanged.emit(commitment)
  }

}
