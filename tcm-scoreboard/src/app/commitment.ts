import { Person } from './person';
import { LeadMeasure } from './leadMeasure';

export class Commitment {
  _id: string;
  person: Person;
  leadMeasure: LeadMeasure;
  statement: string;
  done: boolean;
  createDate: Date;
  completionDate: Date;
}