import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-google-chart',
  templateUrl: './google-chart.component.html',
  styleUrls: ['./google-chart.component.css']
})
export class GoogleChartComponent implements OnChanges {
  @Input() data: Array<Array<any>>

  chart = {
    type: 'BarChart',
    data: [[]],
    options: {
      animation: {
        duration: 250,
        easing: 'ease-in-out',
        startup: true
      },
      legend: 'none',
      width: '100%'
    }

  };

  constructor() { }

  ngOnChanges() {
    this.chart.data = this.data
  }

}
