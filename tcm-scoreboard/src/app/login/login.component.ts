import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router"
import { DataService } from '../data.service';
import { LagMeasure } from '../lagMeasure';

// For posterity
// const triageUrl = "/jira/rest/api/2/search?jql=project%20%3D%20TCM%20AND%20type%20in%20%28%22Integrator%20Submission%22%2C%20Support%29%20AND%20Status%20%21%3D%20Resolved%20ORDER%20BY%20Rank%20ASC&maxResults=0"
// const bugs_20191 = "/jira/rest/api/2/search?jql=project%20%3D%20TCM%20AND%20affectedVersion%20%3D%202019.1%20AND%20%22Dev%20Support%22%20%3D%20%22Dev%20Support%22"
// const bugs_20184 = "/jira/rest/api/2/search?jql=project%20%3D%20TCM%20AND%20affectedVersion%20%3D%202018.4%20AND%20%22Dev%20Support%22%20%3D%20%22Dev%20Support%22"
// const bugs_20183 = "/jira/rest/api/2/search?jql=project%20%3D%20TCM%20AND%20affectedVersion%20%3D%202018.3%20AND%20%22Dev%20Support%22%20%3D%20%22Dev%20Support%22"


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Input() lagMeasures: LagMeasure[]

  username: String
  password: String
  subscriptionStatusMap: Map<string, boolean> = new Map()

  constructor(private dataService: DataService, private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.loadLagMeasures()
  }

  loadLagMeasures() {
    this.dataService.loadLagMeasures().subscribe((savedData: LagMeasure[]) => {
      this.lagMeasures = savedData;

      this.lagMeasures.forEach((lagMeasure) => {
        this.subscriptionStatusMap.set(lagMeasure.name, false)
      })
    });
  }

  onSubmit() {
    let basicAuth = btoa(this.username + ":" + this.password)

		const httpOptions = {
		  headers: new HttpHeaders({
		  	'Access-Control-Allow-Origin': '*',
		    'Authorization': 'Basic ' + basicAuth
		  })
		};

    this.lagMeasures.forEach((lagMeasure) => {
      if (lagMeasure.url) {
        this.http.get(lagMeasure.url, httpOptions).subscribe((result: any) => {
          lagMeasure.value = result.total
          this.subscriptionStatusMap.set(lagMeasure.name, true)
          this.checkSubscriptions()
        }, (error) => {
          console.log(error.status)
        })
      }
    })

  	this.username = ""
  	this.password = ""

  }

  checkSubscriptions() {
    let finished = true

    let newMap = this.subscriptionStatusMap
    newMap.forEach((value) => {
      if (value == false) {
        finished = false
      }
    })

    if (finished) {
      console.log("Done loading")
      this.dataService.updateLagMeasure(this.lagMeasures).subscribe((result: any) => {
        this.router.navigate(["/home"])
      }, (error) => {
        console.log(error)
      })
      
    }
  }

}
