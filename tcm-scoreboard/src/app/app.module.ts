import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { CommitmentLogComponent } from './commitment-log/commitment-log.component';
import { CommitmentComponent } from './commitment/commitment.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, 
  MatTabsModule, 
  MatCheckboxModule, 
  MatFormFieldModule, 
  MatInputModule, 
  MatSelectModule, 
  MatOptionModule,
  MatButtonModule,
  MatListModule,
  MatDividerModule,
  MatExpansionModule,
  MatToolbarModule,
  MatMenuModule,
  MatIconModule } from '@angular/material';
import { GoogleChartComponent } from './google-chart/google-chart.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { AppRoutingModule } from './/app-routing.module';
import { StatisticsComponent } from './statistics/statistics.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    ScoreboardComponent,
    CommitmentLogComponent,
    CommitmentComponent,
    GoogleChartComponent,
    StatisticsComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    GoogleChartsModule.forRoot(),
    MatCardModule,
    MatTabsModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonModule,
    MatCheckboxModule,
    MatListModule,
    MatDividerModule,
    MatExpansionModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
