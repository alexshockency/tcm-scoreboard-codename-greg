import { Goal } from './goal';

export class LeadMeasure {
  _id: string;
  name: string;
  goal: Goal;
}