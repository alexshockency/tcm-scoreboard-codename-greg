import { Goal } from './goal';

export class LagMeasure {
		_id: string;
    name: string;
    goal: Goal;
    url: string;
    value: number;
  }