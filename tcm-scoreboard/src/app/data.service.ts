import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { LagMeasure } from './lagMeasure';
import { Commitment } from './commitment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json; charset=utf-8'
  })
};


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  
  loadPeople() {
    return this.http.get(environment.api.people);
  }
  
  loadLeadMeasures() {
    return this.http.get(environment.api.leadMeasures);
  }
  
  loadCommitments() {
    return this.http.get(environment.api.commitments);
  }

  loadLagMeasures() {
    return this.http.get(environment.api.lagMeasures);
  }
    
  createCommitment(data : Commitment) {
    return this.http.post(environment.api.commitments + '/create', JSON.stringify(data), httpOptions);
  }
  
  updateCommitment(data : Commitment) {
    if (data._id) {
      return this.http.post(environment.api.commitments + '/update', JSON.stringify(data), httpOptions);
    } else {
      console.log('No ID found; cannot update')
      return
    }
  }

  updateLagMeasure(data: LagMeasure[]) {
    return this.http.post(environment.api.lagMeasures + "/update", JSON.stringify(data),  httpOptions);
  }

}
