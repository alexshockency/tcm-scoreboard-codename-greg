import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../data.service';
import { Person } from '../person';
import { LeadMeasure } from '../leadMeasure';
import { Commitment } from '../commitment';
import { LagMeasure } from '../lagMeasure';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  people: Person[]
  leadMeasures: LeadMeasure[]
  commitments: Commitment[]
  lagMeasures: LagMeasure[]

  title = 'tcm-scoreboard'
  
  constructor(private dataService: DataService) { }
  
  ngOnInit() {
    this.loadPeople();
    this.loadLeadMeasures();
    this.loadLagMeasures();
    this.loadCommitments();
  }

  loadPeople() {
    this.dataService.loadPeople().subscribe((savedData: Person[]) => {
      this.people = savedData;
    });
  }
   
  loadLeadMeasures() { 
    this.dataService.loadLeadMeasures().subscribe((savedData: LeadMeasure[]) => {
      this.leadMeasures = savedData;
    });
  }
   
  loadLagMeasures() {
    this.dataService.loadLagMeasures().subscribe((savedData: LagMeasure[]) => {
      this.lagMeasures = savedData;
    });
  }
  
  loadCommitments() { 
    this.dataService.loadCommitments().subscribe((savedData: Commitment[]) => {
      this.commitments = savedData;
    });
  }
  
  createCommitment(commitment: Commitment) {
    this.dataService.createCommitment(commitment).subscribe((data) => {
      this.loadCommitments();
      this.sortCommitments();
    });
  }
  
  updateCommitment(commitment: Commitment) {
    this.dataService.updateCommitment(commitment).subscribe((data) => {
      this.loadCommitments();
      this.sortCommitments();
    });
  }
  
  sortCommitments() {
    this.commitments.sort(function(a,b) {
      if (a.done === b.done) {
        return new Date(b.createDate).getTime() - new Date(a.createDate).getTime()
      } else if (a.done > b.done) {
        return 1
      } else {
        return -1
      }
    })
  }

}
