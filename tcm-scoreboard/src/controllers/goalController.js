const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var Goal = require('../models/goal');

exports.load_goals = function(req, res) {
  Goal.find()
  .then(goals => {
    res.json(goals);
  }).catch(err => {
    res.status(500).send({
      msg: err.message
    });
  });
}

exports.goal_list = function(req, res) {
  Goal.find()
  .sort([['name', 'ascending']])
  .exec(function (err, list_goals) {
    if (err) 
      return next(err);
    
    res.render('goal_list', { goal_list: list_goals });
  });
};

exports.goal_create_get = function(req, res, next) {     
  res.render('goal_form', { });
};

exports.goal_create_post = [
  body('name', 'Name required').isLength({ min: 1 }).trim(),
  
  sanitizeBody('name').trim().escape(),

  (req, res, next) => {
    const errors = validationResult(req);

    var goal = new Goal({
      name: req.body.name
    });

    if (!errors.isEmpty()) {
      res.render('goal_form', {
        goal: goal,
        errors: errors.array()
      });
      return;
      
    } else {
      Goal.findOne({ 'name': req.body.name })
      .exec( function(err, found_goal) {
        if (err)
          return next(err);

        if (found_goal) {
          res.redirect("/admin/goals");
        } else {
          goal.save(function (err) {
            if (err)
              return next(err);
            res.redirect("/admin/goals");
          });
        }
      });
      
    }
  }
];

exports.goal_delete_get = function(req, res, next) {
  // TODO: When there are lead/lag measures tied to a goal, should delete those too
  
  Goal.findOneAndDelete({ _id: req.params.id }, (err) => {
      if (err)
        return next(err);
        
      res.redirect('/admin/goals');
  })
};
