const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var Person = require('../models/person');

exports.load_people = function(req, res) {
  Person.find()
  .then(people => {
    res.json(people);
  }).catch(err => {
    res.status(500).send({
      msg: err.message
    });
  });
}

exports.person_list = function(req, res) {
  Person.find()
  .sort([['name', 'ascending']])
  .exec(function (err, list_people) {
    if (err) 
      return next(err);
    
    res.render('person_list', { person_list: list_people });
  });
};

exports.person_create_get = function(req, res, next) {     
  res.render('person_form', { });
};

exports.person_create_post = [
  body('name', 'Name required').isLength({ min: 1 }).trim(),
  
  sanitizeBody('name').trim().escape(),

  (req, res, next) => {
    const errors = validationResult(req);

    var person = new Person({
      name: req.body.name
    });

    if (!errors.isEmpty()) {
      res.render('person_form', {
        person: person,
        errors: errors.array()
      });
      return;
      
    } else {
      Person.findOne({ 'name': req.body.name })
      .exec( function(err, found_person) {
        if (err)
          return next(err);

        if (found_person) {
          res.redirect("/admin/people");
        } else {
          person.save(function (err) {
            if (err)
              return next(err);
            res.redirect("/admin/people");
          });
        }
      });
      
    }
  }
];

exports.person_delete_get = function(req, res, next) {
  // TODO: When there are commitments tied to a person, should delete those too
  
  Person.findOneAndDelete({ _id: req.params.id }, (err) => {
      if (err)
        return next(err);
        
      res.redirect('/admin/people');
  })
};
