const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var Commitment = require('../models/commitment');
var Person = require('../models/person');
var LeadMeasure = require('../models/leadMeasure');

exports.load_commitments = function(req, res) {
  Commitment.find()
  .populate('person')
  .populate('leadMeasure')
  .then(commitments => {
    res.json(commitments);
  }).catch(err => {
    res.status(500).send({
      msg: err.message
    });
  });
}

exports.commitment_list = function(req, res) {
  Commitment.find()
  .populate('person')
  .populate('leadMeasure')
  .sort([['createDate', 'descending']])
  .exec(function (err, list_commitments) {
    if (err) 
      return next(err);
    
    res.render('commitment_list', { commitment_list: list_commitments });
  });
};

exports.commitment_delete_get = function(req, res, next) {
  Commitment.findOneAndDelete({ _id: req.params.id }, (err) => {
      if (err)
        return next(err);
        
      res.redirect('/admin/commitments');
  })
};

exports.create_commitment = function(req, res, next) {
  var commitment = new Commitment({
    person: req.body.person,
    leadMeasure: req.body.leadMeasure,
    statement: req.body.statement,
    done: req.body.done
  });
  
  commitment.save(function (err) {
    if (err)
      return next(err);
    
    res.json(commitment);
  });
}

exports.update_commitment = function(req, res, next) {
  var data = req.body;
  
  var query = { _id: data._id };
  var updates;
  if (data.done)
    updates = { done: req.body.done, completionDate: Date.now() }
  else
    updates = { done: req.body.done }
  
  Commitment.findOneAndUpdate(query, { $set: updates}, {new: true}, (err, commitment) => {
    if (err)
      return next(err);
    
    res.json(commitment);
  })
}