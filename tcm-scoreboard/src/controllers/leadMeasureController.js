const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');
var async = require('async');

var LeadMeasure = require('../models/leadMeasure');
var Goal = require('../models/goal');

exports.load_lead_measures = function(req, res) {
  LeadMeasure.find()
  .populate('goal')
  .then(lead_measures => {
    res.json(lead_measures);
  }).catch(err => {
    res.status(500).send({
      msg: err.message
    });
  });
}

exports.lead_measure_list = function(req, res) {
  LeadMeasure.find()
  .populate('goal')
  .sort([['goal', 'ascending'], ['name', 'ascending']])
  .exec(function (err, list_lead_measures) {
    if (err) 
      return next(err);
    
    res.render('lead_measure_list', { lead_measure_list: list_lead_measures });
  });
};

exports.lead_measure_create_get = function(req, res, next) {     
  async.parallel({
      goals: function(callback) {
        Goal.find(callback);
      },
    }, function(err, results) {
      if (err)
        return next(err);
      res.render('lead_measure_form', { goals: results.goals });
    });
};

exports.lead_measure_create_post = [
  body('name', 'Name required').isLength({ min: 1 }).trim(),
  body('goal', 'Goal required').isLength({ min: 1 }).trim(),
  
  sanitizeBody('name').trim().escape(),
  sanitizeBody('goal').trim().escape(),

  (req, res, next) => {
    const errors = validationResult(req);

    var lead_measure = new LeadMeasure({
      name: req.body.name,
      goal: req.body.goal
    });

    if (!errors.isEmpty()) {
      res.render('lead_measure_form', {
        lead_measure: lead_measure,
        errors: errors.array()
      });
      return;
      
    } else {
      LeadMeasure.findOne({ 'name': req.body.name })
      .exec( function(err, found_lead_measure) {
        if (err)
          return next(err);

        if (found_lead_measure) {
          res.redirect("/admin/lead_measures");
        } else {
          lead_measure.save(function (err) {
            if (err)
              return next(err);
            res.redirect("/admin/lead_measures");
          });
        }
      });
      
    }
  },
  
  (req, res, next) => next()
];

exports.lead_measure_delete_get = function(req, res, next) {
  // TODO: When there are commitments tied to a lead measure, should delete those too
  
  LeadMeasure.findOneAndDelete({ _id: req.params.id }, (err) => {
      if (err)
        return next(err);
        
      res.redirect('/admin/lead_measures');
  })
};
