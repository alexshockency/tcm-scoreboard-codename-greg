const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');
var async = require('async');

var LagMeasure = require('../models/lagMeasure');
var Goal = require('../models/goal');

exports.load_lag_measures = function(req, res) {
  LagMeasure.find()
  .populate('goal')
  .then(lag_measures => {
    res.json(lag_measures);
  }).catch(err => {
    res.status(500).send({
      msg: err.message
    });
  });
}

exports.lag_measure_list = function(req, res) {
  LagMeasure.find()
  .populate('goal')
  .sort([['goal', 'ascending'], ['name', 'ascending']])
  .exec(function (err, list_lag_measures) {
    if (err) 
      return next(err);
    
    res.render('lag_measure_list', { lag_measure_list: list_lag_measures });
  });
};

exports.lag_measure_create_get = function(req, res, next) {     
  async.parallel({
      goals: function(callback) {
        Goal.find(callback);
      },
    }, function(err, results) {
      if (err)
        return next(err);
      res.render('lag_measure_form', { goals: results.goals });
    });
};

exports.lag_measure_create_post = [
  body('name', 'Name required').isLength({ min: 1 }).trim(),
  body('goal', 'Goal required').isLength({ min: 1 }).trim(),
  
  sanitizeBody('name').trim().escape(),
  sanitizeBody('goal').trim().escape(),

  (req, res, next) => {
    const errors = validationResult(req);

    var lag_measure = new LagMeasure({
      name: req.body.name,
      goal: req.body.goal,
      url: req.body.url,
    });

    if (!errors.isEmpty()) {
      res.render('lag_measure_form', {
        lag_measure: lag_measure,
        errors: errors.array()
      });
      return;
      
    } else {
      LagMeasure.findOne({ 'name': req.body.name })
      .exec( function(err, found_lag_measure) {
        if (err)
          return next(err);

        if (found_lag_measure) {
          res.redirect("/admin/lag_measures");
        } else {
          lag_measure.save(function (err) {
            if (err)
              return next(err);
            res.redirect("/admin/lag_measures");
          });
        }
      });
      
    }
  },
  
  (req, res, next) => next()
];

exports.lag_measure_delete_get = function(req, res, next) {
  // TODO: When there are lead measures tied to a lag measure, should delete those too
  
  LagMeasure.findOneAndDelete({ _id: req.params.id }, (err) => {
      if (err)
        return next(err);
        
      res.redirect('/admin/lag_measures');
  })
};

exports.update_lag_measure = function(req, res, next) {
  var lagMeasures = req.body;
  
  lagMeasures.forEach(lagMeasure => {
    var query = { _id: lagMeasures._id };
    var updates = { value: req.body.value }
  
    LagMeasure.findOneAndUpdate(query, { $set: updates}, {new: true}, (err, lagMeasure) => {
      if (err)
        return next(err);
    })
  })
  
  res.json(lagMeasures);
}