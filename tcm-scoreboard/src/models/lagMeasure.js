var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var LagMeasureSchema = new Schema(
  {
    goal: {type: Schema.Types.ObjectId, ref: 'Goal'},
    name: {type: String, required: true, max: 100},
    url: {type: String},
    value: {type: Number}
  }
);

module.exports = mongoose.model('LagMeasure', LagMeasureSchema);