var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PersonSchema = new Schema(
  {
    name: {type: String, required: true, max: 100},
  }
);

module.exports = mongoose.model('Person', PersonSchema);