var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var CommitmentSchema = new Schema(
  {
    person: {type: Schema.Types.ObjectId, ref: 'Person', required: true},
    leadMeasure: {type: Schema.Types.ObjectId, ref: 'LeadMeasure', required: true},
    statement: {type: String, required: true},
    done: {type: Boolean, required: true},
    createDate: {type: Date, required: true, default: Date.now()},
    completionDate: {type: Date},
  }
);

module.exports = mongoose.model('Commitment', CommitmentSchema);