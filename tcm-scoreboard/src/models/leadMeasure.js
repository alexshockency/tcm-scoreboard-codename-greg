var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var LeadMeasureSchema = new Schema(
  {
    goal: {type: Schema.Types.ObjectId, ref: 'Goal'},
    name: {type: String, required: true, max: 100},
  }
);

module.exports = mongoose.model('LeadMeasure', LeadMeasureSchema);