const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors')
const mongoose = require('mongoose');
const config = require('./config/database');
const fs = require('fs')

const port = 4201

const lagMeasuresPath = './assets/lagMeasures.json'

const dataRouter = require('./routes/data');
const adminRouter = require('./routes/admin');

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

app.set('views', './views');
app.set('view engine', 'pug');

mongoose.connect(config.database);

app.get('/', (req, res) => res.send('Hello TCM! Go to /admin to do things, or /data to see things'))

app.use('/admin', adminRouter)
app.use('/data', dataRouter)

app.listen(port, () => console.log(`Data server listening on port ${port}!`))